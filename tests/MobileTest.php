<?php

namespace Tests;

use App\Call;
use App\Contact;
use App\Exceptions\ContactNotFoundException;
use App\Exceptions\InvalidNumberException;
use App\Interfaces\CarrierInterface;
use App\Mobile;
use App\SMS;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class MobileTest extends TestCase
{
    /** @test */
    public function it_returns_null_when_name_empty()
    {
        $provider = m::mock(CarrierInterface::class);
        $mobile = new Mobile($provider);

        $this->assertNull($mobile->makeCallByName(''));
    }

    /** @test */
    public function it_returns_call_object_when_name_is_valid()
    {
        $provider = m::mock(CarrierInterface::class);
        $provider->shouldReceive('dialContact');
        $provider->shouldReceive('makeCall')
                 ->andReturn(new Call());

        $contactService = m::mock('alias:App\Services\ContactService');
        $contactService->shouldReceive('findByName')
                       ->with('eliecer')
                       ->andReturn(new Contact('eliecer', '123'));
        $mobile = new Mobile($provider);

        $this->assertInstanceOf(Call::class, $mobile->makeCallByName('eliecer'));
    }

    /** @test */
    public function it_returns_a_contact_not_found_exception_when_name_is_not_found()
    {
        $provider = m::mock(CarrierInterface::class);

        $contactService = m::mock('alias:App\Services\ContactService');
        $contactService->shouldReceive('findByName')
            ->with('david')
            ->andReturn(null);
        $mobile = new Mobile($provider);

        $this->expectException(ContactNotFoundException::class);
        $mobile->makeCallByName('david');
    }

    /** @test */
    public function it_returns_an_SMS_object_when_number_is_valid()
    {
        $provider = m::mock(CarrierInterface::class);
        $provider->shouldReceive('sendSMS')
            ->andReturn(new SMS());

        $contactService = m::mock('alias:App\Services\ContactService');
        $contactService->shouldReceive('validateNumber')
            ->with('123')
            ->andReturn(true);
        $mobile = new Mobile($provider);

        $this->assertInstanceOf(SMS::class, $mobile->sendSMS('123', 'test message'));
    }

    /** @test */
    public function it_returns_an_invalid_number_exception_when_number_is_invalid()
    {
        $provider = m::mock(CarrierInterface::class);

        $contactService = m::mock('alias:App\Services\ContactService');
        $contactService->shouldReceive('validateNumber')
            ->with('123')
            ->andReturn(false);
        $mobile = new Mobile($provider);

        $this->expectException(InvalidNumberException::class);
        $mobile->sendSMS('123', 'test message');
    }

    public function tearDown(): void
    {
        parent::tearDown();

        m::close();
    }
}
