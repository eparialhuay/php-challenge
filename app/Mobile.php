<?php

namespace App;

use App\Exceptions\ContactNotFoundException;
use App\Exceptions\InvalidNumberException;
use App\Interfaces\CarrierInterface;
use App\Services\ContactService;
use http\Exception\InvalidArgumentException;


class Mobile
{

	protected $provider;
	
	function __construct(CarrierInterface $provider)
	{
		$this->provider = $provider;
	}

    public function makeCallByName($name = '')
    {
        if (empty($name)) return;

        $contact = ContactService::findByName($name);

        if (is_null($contact)) {
            throw new ContactNotFoundException();
        }

        $this->provider->dialContact($contact);

        return $this->provider->makeCall();
    }

    public function sendSMS(string $number, string $body)
    {
        if (empty($number)) return;

        if (! ContactService::validateNumber($number)) {
            throw new InvalidNumberException();
        }

        return $this->provider->sendSMS($number, $body);
    }
}