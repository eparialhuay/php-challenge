<?php

namespace App;

use App\Interfaces\CarrierInterface;

class OtherMobileCarrier implements CarrierInterface
{
    public function dialContact(Contact $contact)
    {
        // TODO: Implement dialContact() method.
    }

    public function makeCall(): Call
    {
        // TODO: Implement makeCall() method.
    }

    public function sendSMS(string $number, string $body): SMS
    {
        // TODO: Implement sendSMS() method.
    }
}